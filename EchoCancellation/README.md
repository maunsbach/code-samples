## Adaptive Filtering for Acoustic Echo Cancellation

When a phone conversation is heard through a loudspeaker, the signal can echo back to the source. The adaptive filtering aims to cancel out this echo by using linear prediction on the source to filter the receivers signal, removing the echo.