# Code Samples ([Show Reel Link](https://youtu.be/UKFNE_P-v_Y))

Included are 4 sample projects with digital signal processing in C++, C# and MatLab. Explanatory README files are in each folder. Short descriptions follow here.

### EKS (C++, MatLab)
An advanced Karplus-Strong implementation used for an interactive instrument. Filter design.

### Echo Cancellation (MatLab)
Adaptive filtering to cancel out an acoustic echo in phone conversations that pick up the sound from their own speaker.

### Friction (C++, MatLab)
Advanced Friction synthesis using K method, trapezoidal rule and Newton-Rhapson to approximate a non-linearity. 

### Impact (C#, MatLab)
Impact and rolling synthesis using K method, trapezoidal rule and Newton-Rhapson. 

A complete synthesis toolkit for the Unity game engine is implemented.

Banded Waveguide impact synthesis is implemented using damped white noise through band pass filters.